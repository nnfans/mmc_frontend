// Userbank Library
// --irfan

var Promise = require('bluebird');
var unirest =  require('unirest');
var g_api = process.env.UB_KEY;

var uri = 'http://' + process.env.UB_HOST + ':' + process.env.UB_PORT;

var clientGet = unirest( 'GET', {
  headers: {
    'Accept': 'application/json'
  }
});

var clientPost = unirest( 'POST', {
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/x-www-form-urlencoded'
  }
});


// User login and get token
function password ( user, password ) {

  // return new promise
  return new Promise( function( resolve, reject ) {

    clientPost.url( uri + '/user/auth/password' )
    .send({
      username: user,
      password: password
    })
    .end( res => {

      if ( !res.body  ) {
        reject( res );
        return;

      } else {
        if ( !res.body.success ) {  // If status is not success reject Promise
          reject( res.body.msg );
          return;
        }
      }

      // res.body.user[permissions] = res.body.permissions;
      // res.body.user[roles] = res.body.roles;

      resolve( res.body )  // Resolve promise and pass user variables
    });

  });
};

// save token in session
function token( username, token ) {

  // return new promise
  return new Promise( function( resolve, reject ) {

    clientPost.url( uri + '/user/auth/token' )
    .send({
      username: username,
      token: token
    })
    .end( res => {

      if ( !res.body  ) {
        reject( res );
        return;

      } else {
        if ( !res.body.success ) {  // If status is not success reject Promise
          reject( res.body.msg );
          return;
        }
      }

      if ( !res.body.user ) {  // If token is not match
        reject( 'Mismatch token');
        return;
      }

      resolve( res.body.user )  // Resolve promise and pass user variables
    });
  });

};

// Check permission
function checkPermission( username, permission ){

  // return new promise
  return new Promise( function( resolve, reject ) {

    clientPost.url( uri + '/user/auth/permission' )
    .send({
      username: username,
      permission: permission,
      group_api: g_api
    })
    .end( res => {
      if ( !res.body  ) {
        reject( res );
        return;

      } else {
        if ( !res.body.success ) {  // If status is not success reject Promise
          reject( res.body.msg );
          return;
        }
      }

      if ( !res.body.result ) {  // If token is not match
        reject( 'No permission for current user' );
        return;
      }

      resolve( res.body.user );  // Resolve promise and pass user variables
    });
  });

};

// Check permission generator
function checkPermission_g( permission ) {

  return function ( username ) {

    if ( typeof username === 'object' ){
      username = username.username;
    }

    // return new promise
    return checkPermission( username, permission );
  }
}

module.exports = {

  password: password,
  token: token,
  checkPermission: checkPermission,
  checkPermission_g: checkPermission_g
};
