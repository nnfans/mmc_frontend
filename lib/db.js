// Load DB Builder by knex
// -- Irfan

var knex = require('knex');

module.exports = knex({
  client: process.env.DB_DRIVER,
  connection: {
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME
  },
  debug: true,
  pool: {
    afterCreate: function (conn, done) {
      // in this example we use pg driver's connection API
      conn.query("SET TIMEZONE TO 'Asia/Jakarta';", function (err) {
          done(err, conn);
      });
    }
  }
});  // Export knex connection
