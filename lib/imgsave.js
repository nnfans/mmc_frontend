// Imgsave Library
// --irfan

var Promise = require('bluebird');
var imgPath = '/dist/storage/images/';
var fs = require('fs');

function saveBase64 ( data ) {
  return new Promise ( function ( resolve, reject ) {
    var name = new Date().getTime() + '_' + (Math.random() * 10000).toFixed() + '.jpg';
    var base64Data = data.replace(/^data:image\/jpeg;base64,/, '');

      fs.writeFileSync(appRoot + imgPath + name, base64Data, 'base64', function(err) {

        resolve( imgPath + name );
      });
  });
}

function quickSaveBase64 ( data ) {
  var name = new Date().getTime() + '_' + (Math.random() * 10000).toFixed() + '.jpg';
  var base64Data = data.replace(/^data:image\/jpeg;base64,/, '');

  fs.writeFileSync(appRoot + imgPath + name, base64Data, 'base64', function(err) {

  });

  return imgPath + name;
}

function saveBase64Sync ( data ) {
  var name = new Date().getTime() + '_' + (Math.random() * 10000).toFixed() + '.jpg';
  var base64Data = data.replace(/^data:image\/jpeg;base64,/, '');

  fs.writeFileSync(appRoot + imgPath + name, base64Data, 'base64')

  return imgPath + name;
}

module.exports = {
  saveBase64,
  quickSaveBase64,
  saveBase64Sync
};