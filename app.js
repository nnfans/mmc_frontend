// app.js
// --irfan

require('dotenv').config();  // Load env configuration

var path = require('path');  // Load path module
var os = require('os');  // Load os module
var fs = require('fs');  // Load filesystem module

global.appRoot = path.resolve(__dirname);  // Set appRoot to current root project directory

var restify = require('restify');  // restify core
var Router = require('restify-router').Router;  // better router for restify
var CookieParser = require('restify-cookies');  // restify cookie plugin

restify.plugins = require('restify-plugins');  // Inject restify plugins

const corsMiddleware = require('restify-cors-middleware')  // Allowing CORS

const cors = corsMiddleware({
  preflightMaxAge: 5, //Optional
  origins: [ 'http://localhost:8080', 'http://107.102.29.158:8080' ],
  allowHeaders: ['API-Token'],
  exposeHeaders: ['API-Token-Expiry']
})

var db = require( appRoot + '/lib/db');  // Load db SQL builder
var routes = require('./routes');  // Load routes

var routerInstance = new Router();  // Create new routerInstance

// Create restify server
var server = restify.createServer({
    name: 'pandora',
    version: '0.10.0',
    key: fs.readFileSync('./cert/107.102.29.158.key'),
    certificate: fs.readFileSync('./cert/107.102.29.158.crt')
});

server.use( restify.plugins.queryParser() );
server.use( restify.plugins.bodyParser({
    maxBodySize: 0,
    mapParams: true,
    mapFiles: false,
    overrideParams: false,
    keepExtensions: false,
    uploadDir: os.tmpdir(),
    multiples: true,
    hash: 'sha2',
    rejectUnknown: true,
    requestBodyOnGet: false,
    reviver: undefined,
    maxFieldsSize: 50 * 1024 * 1024
 }));

 server.pre(cors.preflight)
 server.use(cors.actual)

 server.use( CookieParser.parse );  // restify cookie parser

 server.use( function( req, res, next ) {
   if ( !req.body ){
     req.body = {};
   };
   res.header('Access-Control-Allow-Credentials', true)
   return next();
 });

 server.get(/\/dist\/?.*/, restify.plugins.serveStatic({
   directory: appRoot,
   default: 'index.html'
 }));

routes.applyRoutes( server, '/' );  // Apply router to server

server.listen( process.env.SERVER_PORT, function() {  // Start listening port
  console.log('%s @%s listening at port %s', server.name, server.versions, process.env.SERVER_PORT);
});
