// mactype routes
// --irfan

var Promise = require('bluebird');
var Router = require('restify-router').Router;  // Restify router modules
var moment = require('moment');  // Date Time Modules
var validate = require("validate.js");  // Data validator

var userbank = require( appRoot + '/lib/userbank' );
var db = require( appRoot + '/lib/db' );  // Load knex sql builder

const router = new Router();  // Create new router

// List all Movement
router.get('/', ( req, res, next ) => {

  var cookies = req.cookies;

  var username = cookies.username;
  var login_token = cookies.login_token;

  var id = req.query.id;

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  userbank.token( username, login_token )
  .then( userbank.checkPermission_g( 'location_change' ) )

  .then( function() {

    if ( id ) {

      return db( 'location_change_histories' )
      .where( 'machine_code', id )
      .orderBy( 'id', 'desc' )
      .select( 'location' )
      .limit(1);
    } else {

      return db( 'location_change_histories' )
      .select( '*' );
    }
  })
  .then( function( rows ) {

    if ( id ) {

      res.send({ success: true, location: rows[0].location });
    } else {

      res.send({ success: true, data: rows });
    }
    return next();
  })
  .catch( function( err ) {

    res.send({ success: false, msg: err });
    return next();
  });

});

// Move
router.post( '/', ( req, res, next ) => {

  var cookies = req.cookies;

  var username = cookies.username;
  var login_token = cookies.login_token;
  var new_location = req.body;

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  userbank.token( username, login_token )
  .then( function(user) {
    new_location.created_by = user.id;

    return user;
  })
  .then( userbank.checkPermission_g( 'location_change' ) )
  .then( function() {

    if ( new_location.type == 'machine' ) {

      return db( 'machines' )
      .where( 'barcode', new_location.machine_code )
      .returning( '*' )
      .update({
        location_id: new_location.location_id
      });
    }

    return db( 'assets' )
    .where( 'sn', new_location.machine_code )
    .returning( '*' )
    .update({
      location_id: new_location.location_id,
      process: new_location.process
    });
  })
  .then( function() {
    // Get Location name from db

    return db( 'locations' )
    .where( 'id', new_location.location_id )
    .select( 'name' )
    .limit( 1 );
  })
  .then( function( r ) {
    delete new_location.location_id;
    delete new_location.type;
    new_location.location = r[0].name;

    return db( 'location_change_histories' )
    .returning( '*' )
    .insert( new_location );
  })
  .then( function( r ) {

    res.send({ success: true, new_location: r[0] });
    return next();
  })

});


module.exports = router;
