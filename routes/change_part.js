// Change Part routes
// --irfan

var Promise = require('bluebird');
var Router = require('restify-router').Router;  // Restify router modules
var moment = require('moment');  // Date Time Modules
var validate = require("validate.js");  // Data validator

var userbank = require( appRoot + '/lib/userbank' );
var db = require( appRoot + '/lib/db' );  // Load knex sql builder

const router = new Router();  // Create new router

// Check new Change Part
function checkNewChangePart( new_changepart ) {

  return function () {

    return new Promise ( ( resolve, reject ) => {

      var rules = {

        machine_id: {
          presence: {
            allowEmpty: false
          },
          numericality: {
            onlyInteger: true
          }
        },
        part: {
          presence: {
            allowEmpty: false
          },
          length: {
            maximum: 40
          }
        },
        created_by: {
          numericality: {
            onlyInteger: true
          }
        },
        remark: {
          length: {
            maximum: 30
          }
        }

      };

      var err = validate( new_changepart, rules);

      if ( err ) {
        reject( err );
        return;
      }

      resolve( new_changepart );

    });  // Promise
  }  // function

};

// List all Change Part
router.get('/', ( req, res, next ) => {

  var cookies = req.cookies;

  var username = cookies.username;
  var login_token = cookies.login_token;

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  userbank.token( username, login_token )
  .then( userbank.checkPermission_g( 'part_change' ) )

  .then( function() {

    return db( 'part_changes as pc' )
    .join( 'machines as m', 'pc.machine_id', 'm.id' )
    .where( 'pc.created_at', '>', moment().startOf('day') )
    .select([
      'pc.created_at as created_at',
      'm.barcode as barcode',
      'pc.part as part',
      'pc.lifetime as lifetime',
      'pc.remark as remark'
    ]);
  })
  .then( function( rows ) {

    res.send({ success: true, data: rows });
    return next();
  })
  .catch( function( err ) {

    res.send({ success: false, msg: err });
    return next();
  });

});

// Create new Change Part
router.post( '/', ( req, res, next ) => {

  var cookies = req.cookies;

  var username = cookies.username;
  var login_token = cookies.login_token;

  var new_changepart = req.body;

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  userbank.token( username, login_token )
  .then( function(user) {
    new_changepart.created_by = user.id;

    return user;
  })
  .then( userbank.checkPermission_g( 'part_change' ) )
  .then( checkNewChangePart( new_changepart ) )  // Check change parts vars
  .then( function( data ) {

    return db( 'part_changes' )
    .returning( '*' )
    .insert( data );

  })
  .then( function( changepart ) {

    res.send({ success: true, changepart: changepart });
    return next();
  })
  .catch( function( err ) {

    res.send({ success: false, msg: err });
    return next();
  });

});

module.exports = router;
