// machines routes
// --irfan

var Promise = require('bluebird');
var Router = require('restify-router').Router;  // Restify router modules
var moment = require('moment');  // Date Time Modules
var validate = require("validate.js");  // Data validator

var userbank = require( appRoot + '/lib/userbank' );
var db = require( appRoot + '/lib/db' );  // Load knex sql builder

const router = new Router();  // Create new router

// Check Machines Barcode
function checkBarcode ( barcode ) {

  return new Promise ( ( resolve, reject ) => {

    if ( barcode ) {

      db( 'machines' )
      .where( 'barcode', barcode )
      .select( 'id' )
      .limit(1)
      .asCallback( function ( err, rows ){

        if ( err ) {
          reject( err );
          return;
        };

        if ( rows[0] ) {

          reject( {
            barcode: [
              'Barcode has been exists.'
            ]
          });
          return;
        } else {
          resolve();
        };

      });  // Callback

    } else {
      resolve();
    }

  });  // Promise

};

// Check new Machine
function checkNewMachine( new_machine ) {

  return function () {

    return new Promise ( ( resolve, reject ) => {

      var rules = {

        barcode: {
          presence: {
            allowEmpty: false
          },
          length: {
            maximum: 15
          }
        },
        location_id: {
          presence: {
            allowEmpty: false
          },
          numericality: {
            onlyInteger: true
          }
        },
        mactype_id: {
          presence: {
            allowEmpty: false
          },
          numericality: {
            onlyInteger: true
          }
        },
        remark: {
          length: {
            maximum: 30
          }
        },
        created_by: {
          numericality: {
            onlyInteger: true
          }
        },
        updated_by: {
          numericality: {
            onlyInteger: true
          }
        }

      };

      var err = validate( new_machine, rules);

      if ( err ) {
        reject( err );
        return;
      }

      checkBarcode( new_machine.barcode )
      .then( function () {
        resolve( new_machine );
      })
      .catch( function ( err ) {
        reject( err );
      });

    });  // Promise
  }  // function

};

// Check mod Machine
function checkModMachine( mod_machine ) {

  return function () {

    return new Promise ( ( resolve, reject ) => {

      var rules = {

        barcode: {
          length: {
            maximum: 15
          }
        },
        location_id: {
          numericality: {
            onlyInteger: true
          }
        },
        mactype_id: {
          numericality: {
            onlyInteger: true
          }
        },
        remark: {
          length: {
            maximum: 30
          }
        },
        created_by: {
          numericality: {
            onlyInteger: true
          }
        },
        updated_by: {
          numericality: {
            onlyInteger: true
          }
        }

      };

      var err = validate( mod_machine, rules);

      if ( err ) {
        reject( err );
        return;
      }

      checkBarcode( mod_machine.barcode )
      .then( function () {
        resolve( mod_machine );
      })
      .catch( function ( err ) {
        reject( err );
      });

    });  // Promise
  }  // function

};

// List all machines
router.get( '/', ( req, res, next ) => {

  var cookies = req.cookies;

  var username = cookies.username;
  var login_token = cookies.login_token;

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  userbank.token( username, login_token )
  .then( userbank.checkPermission_g( 'machines_list' ) )

  .then( function() {

    return db( 'machines as m' )
    .innerJoin( 'mactypes as mt', 'm.mactype_id', 'mt.id' )
    .innerJoin( 'locations as l', 'm.location_id', 'l.id' )

    .whereNull( 'm.deleted_at' )

    .select([
      'm.id as id',
      'm.barcode as barcode',
      'm.mactype_id as mactype_id',
      'mt.name as name',
      'mt.type as type',
      'mt.spec as spec',
      'm.location_id as location_id',
      'l.name as location',
      'l.type as location_type',
      'm.remark as remark',
      'm.created_at as created_at',
      'm.updated_at as updated_at'
    ]);
  })
  .then( function( rows ) {

    res.send({ success: true, data: rows });
    return next();
  })
  .catch( function( err ) {

    res.send({ success: false, msg: err });
    return next();
  });

});

// Selection for mactypes
router.get('/selection', ( req, res, next ) => {

  var cookies = req.cookies;
  var mactype_id = req.query.mactype_id;

  var username = cookies.username;
  var login_token = cookies.login_token;

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  db( 'machines' )
  .where( 'mactype_id', mactype_id )
  .select('barcode')
  .then( function( rows ) {

    rows = rows.map( function( item ) {
      return item.barcode;
    });

    res.send({ success: true, data: rows });
    return next();
  })
  .catch( function( err ) {

    res.send({ success: false, msg: err });
    return next();
  });
  
});

// List all machines
router.get( '/:barcode', ( req, res, next ) => {
  
    var cookies = req.cookies;
  
    var barcode = req.params.barcode;
    var username = cookies.username;
    var login_token = cookies.login_token;
  
    if ( !username || !login_token ) {
      res.send({ success: false });
      return next();
    };
  
    userbank.token( username, login_token )
    .then( userbank.checkPermission_g( 'machines_list' ) )
  
    .then( function() {
  
      return db( 'machines as m' )
      .innerJoin( 'mactypes as mt', 'm.mactype_id', 'mt.id' )
      .innerJoin( 'locations as l', 'm.location_id', 'l.id' )
  
      .where('barcode', barcode)
  
      .select([
        'm.id as id',
        'm.barcode as barcode',
        'm.mactype_id as mactype_id',
        'mt.name as name',
        'mt.type as type',
        'mt.spec as spec',
        'm.location_id as location_id',
        'l.name as location',
        'l.type as location_type',
        'm.remark as remark',
        'm.created_at as created_at',
        'm.updated_at as updated_at'
      ]);
    })
    .then( function( rows ) {

      if ( rows ) {
        
        res.send({ success: true, data: rows[0] });
        return next();
      }

      res.send({ success: false, data: `No machine with code: ${barcode}` });
      return next();

    })
    .catch( function( err ) {

      res.send({ success: false, msg: err });
      return next();
    });

  });

// Create new machines
router.post( '/', ( req, res, next ) => {

  var cookies = req.cookies;

  var username = cookies.username;
  var login_token = cookies.login_token;
  var new_machine = req.body;

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  userbank.token( username, login_token )
  .then( function(user) {
    new_machine.created_by = user.id;
    new_machine.updated_by = user.id;

    return user;
  })
  .then( userbank.checkPermission_g( 'machines_create' ) )
  .then( checkNewMachine( new_machine ) )  // Check mactype vars
  .then( function( data ) {

    return db( 'machines' )
    .returning( '*' )
    .insert( data );
  })
  .then( function( machine ) {

    res.send({ success: true, machine: machine });
    return next();
  })
  .catch( function( err ) {

    res.send({ success: false, msg: err });
    return next();
  });

});

// Update machines
router.patch( '/', ( req, res, next ) => {

  var cookies = req.cookies;

  var username = cookies.username;
  var login_token = cookies.login_token;
  var mod_machine = req.body;

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  userbank.token( username, login_token )
  .then( function(user) {
    mod_machine.updated_by = user.id;
    mod_machine.updated_at = moment();

    return user;
  })
  .then( userbank.checkPermission_g( 'machines_manage' ) )
  .then( checkModMachine( mod_machine ) )  // Check machine vars
  .then( function( data ) {
    var id = data.id;
    delete data.id;

    return db( 'machines' )
    .where( 'id', id )
    .returning( '*' )
    .update( data );
  })
  .then( function( result ) {

    res.send({ success: true, machine: result });
    return next();
  })
  .catch( function( err ) {

    res.send({ success: false, msg: err });
    return next();
  });

});

// Delete machines
router.del( '/', ( req, res, next ) => {

  var cookies = req.cookies;

  var username = cookies.username;
  var login_token = cookies.login_token;
  var machine_id = req.body.id;
  var machine = {};

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  userbank.token( username, login_token )
  .then( function(user) {
    machine.updated_by = user.id;
    machine.updated_at = moment();
    machine.deleted_at = moment();

    return user;
  })
  .then( userbank.checkPermission_g( 'machines_manage' ) )
  .then( function() {

    var machines = db( 'machines' );
    if ( typeof machine_id === 'object' ) {
      machines = machines.whereIn( 'id', machine_id )
    } else {
      machines = machines.where( 'id', machine_id )
    }

    return machines.returning( '*' )
    .update( machine );
  })
  .then( function( result ) {

    res.send({ success: true, machine: result });
    return next();
  })
  .catch( function( err ) {

    res.send({ success: false, msg: err });
    return next();
  });

});

module.exports = router;
