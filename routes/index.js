// index routes
// --irfan

var Router = require('restify-router').Router;  // Restify router modules
var restify_plugins = require('restify-plugins');
var userbank = require( appRoot + '/lib/userbank' );

const router = new Router();  // Create new router

router.get('/', restify_plugins.serveStatic({
  directory: appRoot,
  default: 'index.html'
}));

router.post('/login', ( req, res, next ) => {

  var username = req.body.username;
  var password = req.body.password;

  userbank.password( username, password )
  .then( function( data ) {

    if ( !data ) {
      throw 'Userbank API Login user error';
    }

    res.setCookie( 'name', data.user.name );
    res.setCookie( 'username', data.user.username );
    res.setCookie( 'login_token', data.user.login_token );

    res.send({ success: true, user: {
      name: data.user.name,
      username: data.user.username,
      login_token: data.user.login_token,
      created_at: data.user.created_at,
      updated_at: data.user.updated_at,
      permissions: data.permissions,
      roles: data.roles
    }});
    return next();

  })
  .catch( function( err ) {

    res.clearCookie( 'name' );
    res.clearCookie( 'username' );
    res.clearCookie( 'login_token' );

    res.send({ success: false, msg: err });
    return next();
  });
});

router.add("/mactypes", require("./mactypes"));
router.add("/locations", require("./locations"));
router.add("/machines", require("./machines"));
router.add("/smfm", require("./smfm"));
router.add("/pm", require("./pm"));
router.add("/change_part", require("./change_part"));
router.add("/assets", require("./assets"));
router.add("/move", require("./move"));
router.add("/change_part", require("./change_part"));
router.add("/result", require("./result"));

module.exports = router;  // Export routes
