// smfm routes
// --irfan

var Router = require('restify-router').Router;  // Restify router modules
var userbank = require( appRoot + '/lib/userbank' );

const router = new Router();  // Create new router

router.add('/heads', require('./heads'));
router.add('/items', require('./items'));
router.add('/reports', require('./reports'));

module.exports = router;  // Export routes
