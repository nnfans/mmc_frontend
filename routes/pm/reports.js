// PM Head routes
// --irfan

var fs = require('fs');
var Promise = require('bluebird');
var Router = require('restify-router').Router;  // Restify router modules
var moment = require('moment');  // Date Time Modules
var xl = require('excel4node');

var userbank = require( appRoot + '/lib/userbank' );
var db = require( appRoot + '/lib/db' );  // Load knex sql builder

const router = new Router();  // Create new router

var styles = (function() {

  function newObj ( obj ) {
    return JSON.parse( JSON.stringify(obj) );
  }

  const blue = '#0000FF';
  const defaultBorder = 'thin';

  var centerAlign = {
    alignment: {
      horizontal: 'center',
      vertical: 'center'
    }
  };

  var blueB = {
    border: {
      bottom: {
        style: defaultBorder,
        color: blue
      }
    }
  }

  var blueBox = {
    border: {
      left: {
        style: defaultBorder,
        color: blue
      },
      right: {
        style: defaultBorder,
        color: blue
      },
      top: {
        style: defaultBorder,
        color: blue
      },
      bottom: {
        style: defaultBorder,
        color: blue
      }
    }
  };

  var blueBoxOuter = {
    border: {
      left: {
        style: 'medium',
        color: blue
      },
      right: {
        style: 'medium',
        color: blue
      },
      top: {
        style: 'medium',
        color: blue
      },
      bottom: {
        style: 'medium',
        color: blue
      },
      outline: true
    }
  };

  var blueR = {
    border: {
      right: {
        style: defaultBorder,
        color: blue
      }
    }
  };

  var blueL = {
    border: {
      left: {
        style: defaultBorder,
        color: blue
      }
    }
  };

  var blueB = {
    border: {
      bottom: {
        style: defaultBorder,
        color: blue
      }
    }
  };

  var blueLB = {
    border: {
      left: {
        style: 'medium',
        color: blue
      }
    }
  };

  var blueRB = {
    border: {
      right: {
        style: 'medium',
        color: blue
      }
    }
  };

  var blueBB = {
    border: {
      bottom: {
        style: 'medium',
        color: blue
      }
    }
  };

  var bold = {
    font: {
      bold: true
    }
  };

  var blueBoxCenter = Object.assign( newObj( blueBox ), centerAlign );

  var headerTop = Object.assign( newObj(blueBoxCenter), bold );
  headerTop.border.top.style = 'medium';

  var headerBottom = Object.assign( newObj(blueBoxCenter), bold );
  headerBottom.border.bottom.style = 'double';

  var headerBoth = Object.assign( newObj(headerTop), bold );
  headerBoth.border.bottom.style = 'double';

  return {
    bold,
    blueBox,
    blueBoxOuter,
    blueL,
    blueR,
    blueB,
    blueRB,
    blueLB,
    blueBB,
    blueBoxCenter,
    headerTop,
    headerBottom,
    headerBoth,
    centerAlign,
    blueBox
  };
}());

function makePmReport ( datas ) {

  function r ( v ) {
    return Number( v ) + 1;
  }

  function c ( v ) {
    return Number( v ) + 1;
  }

  var wb = new xl.Workbook({
    defaultFont: {
      size: 10,
      name: 'Arial',
      color: '#0000FF'
    }
  });

  var options = {
    margins: {
      left: 0.21,
      right: 0.21,
      top: 0.14,
      bottom: 0.14
    },
    sheetView: {
      showGridLines: false,
    },
    sheetFormat: {
      defaultColWidth: 4,
      defaultRowHeight: 13
    }
  };

  var ws = wb.addWorksheet('PM', options);

  ws.column(1).setWidth(1);
  ws.column(c(27)).setWidth(1);
  ws.row(1).setHeight(1);

  // ## Logo and caption
  // Samsung Logo
  ws.addImage({
    path: appRoot + '/resource/images/samsung_logo.png',
    type: 'picture',
    position: {
      type: 'oneCellAnchor',
      from: {
        col: c(1),
        colOff: 0,
        row: r(1),
        rowOff:0
      }
    }
  });

  ws.cell( r(4) , c(1) ).string('PT. Samsung Electronics Indonesia');
  ws.cell( r(5), c(1) ).string('HHP Mobile');


  // ## Signature box
  // Header
  ws.cell( r(1), c(18), r(1), c(20), true )
  .string('Drafter').style( styles.blueBoxCenter );
  ws.cell( r(1), c(21), r(1), c(23), true )
  .string('Check').style( styles.blueBoxCenter );
  ws.cell( r(1), c(24), r(1), c(26), true )
  .string('Ass Mgr').style( styles.blueBoxCenter );

  // Empty Content
  ws.cell( r(2), c(18), r(7), c(20), true ).style( styles.blueBox );
  ws.cell( r(2), c(21), r(7), c(23), true ).style( styles.blueBox );
  ws.cell( r(2), c(24), r(7), c(26), true ).style( styles.blueBox );

  // Footer name
  ws.cell( r(8), c(18), r(8), c(20), true )
  .string('Nugroho SP').style( styles.blueBoxCenter );
  ws.cell( r(8), c(21), r(8), c(23), true )
  .string('Ilham M').style( styles.blueBoxCenter );
  ws.cell( r(8), c(24), r(8), c(26), true )
  .string('A. Djaka Y').style( styles.blueBoxCenter );

  // ## Add Sign Watermark
  ws.addImage({
    path: appRoot + '/resource/images/sign_watermark.png',
    type: 'picture',
    position: {
      type: 'oneCellAnchor',
      from: {
        col: c(18),
        colOff: '3.5mm',
        row: r(2),
        rowOff: '2mm'
      }
    }
  });

  // ## Add PM Title
  ws.addImage({
    path: appRoot + '/resource/images/pm_title.png',
    type: 'picture',
    position: {
      type: 'oneCellAnchor',
      from: {
        col: c(5),
        colOff: '2mm',
        row: r(7),
        rowOff: '1.5mm'
      }
    }
  });

  // ## Captions
  // Subtitle
  ws.cell( r(11), c(1) ).string('HHP Mobile Maintenance Team').style(styles.bold);

  // Date
  ws.cell( r(11), c(26) ).string( 'Date: ' + datas.date.format('DD MMMM YYYY') )
  .style({
    alignment: {
      horizontal: 'right'
    }
  });

  // ## First Table
  // Header
  ws.cell( r(12), c(1) ).string("No").style( styles.headerBoth );
  ws.cell( r(12), c(2), r(12), c(11), true ).string('Description').style( styles.headerBoth );
  ws.cell( r(12), c(12), r(12), c(19), true ).string('Tools').style( styles.headerBoth );
  ws.cell( r(12), c(20), r(12), c(23), true ).string('Line').style( styles.headerBoth );
  ws.cell( r(12), c(24), r(12), c(26), true ).string('Executor').style( styles.headerBoth );

  // Machine Details
  var currRow = 13;
  ws.cell( r(currRow), c(1) ).number(1).style( styles.centerAlign );
  ws.cell( r(currRow), c(2) ).string('Machine  :');
  ws.cell( r(currRow + 1), c(2) ).string('QR Code :');
  ws.cell( r(currRow), c(5) ).string(datas.machine_name);
  ws.cell( r(currRow + 1), c(5) ).string(datas.machine_id);
  ws.cell( r(currRow + 3), c(2) ).string('PM Item:');

  // Items
  var items = Object.keys( datas.items );
  items.forEach( ( item, i ) => {
    ws.cell( r(currRow + 3 + i), c(4) ).string( (i + 1) + '. ' + item );
  });

  // Tools
  datas.tools.forEach( ( tool, i ) => {
    ws.cell( r(currRow + i), c(12) ).string( '~ ' + tool );
  });

  // Borders
  var lastRow = Math.max( items.length + 2, datas.tools.length) + currRow + 1;
  ws.cell( r(currRow), c(1), r(lastRow), c(1) ).style( styles.blueR );  // Number border
  ws.cell( r(currRow), c(11), r(lastRow), c(11) ).style( styles.blueR );  // Description border
  ws.cell( r(currRow), c(19), r(lastRow), c(19) ).style( styles.blueR );  // Tools border
  ws.cell( r(currRow), c(23), r(lastRow), c(23) ).style( styles.blueR );  // Line border
  ws.cell( r(currRow - 1), c(1), r(lastRow), c(1) ).style( styles.blueLB );  // Outer medium left border
  ws.cell( r(currRow - 1), c(26), r(lastRow), c(26) ).style( styles.blueRB );  // Outer medium right border
  ws.cell( r(lastRow), c(1), r(lastRow), c(26) ).style( styles.blueBB );  // Outer medium bottom border
  ws.cell( r(currRow), c(20), r(lastRow), c(23), true ).string( datas.line ).style( styles.centerAlign ); // Line
  ws.cell( r(currRow), c(24), r(lastRow), c(26), true ).string( datas.executor ).style( styles.centerAlign ); // Executor

  // ## Second Table
  currRow = lastRow + 2;

  // Header
  ws.cell( r(currRow), c(1), r(currRow + 1), c(1), true ).string('No').style( styles.headerBoth );
  ws.cell( r(currRow), c(2), r(currRow), c(26), true ).string('Action / Activity').style( styles.headerTop );
  ws.cell( r(currRow + 1), c(2), r(currRow + 1), c(11), true ).string('Before').style( styles.headerBottom );
  ws.cell( r(currRow + 1), c(12), r(currRow + 1), c(21), true ).string('After').style( styles.headerBottom );
  ws.cell( r(currRow + 1), c(22), r(currRow + 1), c(26), true ).string('Remark').style( styles.headerBottom );

  // Content
  currRow += 2;
  var mRow = currRow;
  var data = datas.items;

  items.forEach( ( name, i ) => {

    // Add Before Image
    if ( data[name].before_img ) {
      try {
        ws.addImage({
          path: appRoot + data[name].before_img,
          type: 'picture',
          position: {
            type: 'twoCellAnchor',
            from: {
              col: c(2),
              colOff: '2mm',
              row: r(mRow),
              rowOff: '1.5mm'
            },
            to: {
              col: c(11),
              colOff: '2mm',
              row: c(mRow + 13),
              rowOff: '1.5mm'
            }
          }
        });
      } catch ( e ) {
        console.log( 'Errors: PM Reports add before image: ' + e );
      }
    }

    // Add After Image
    if ( data[name].after_img ) {
      try {
        ws.addImage({
          path: appRoot + data[name].after_img,
          type: 'picture',
          position: {
            type: 'twoCellAnchor',
            from: {
              col: c(2),
              colOff: '2mm',
              row: r(mRow),
              rowOff: '1.5mm'
            },
            to: {
              col: c(11),
              colOff: '2mm',
              row: c(mRow + 13),
              rowOff: '1.5mm'
            }
          }
        });
      } catch ( e ) {
        console.log( 'Errors: PM Reports add after image: ' + e );
      }
    }

    // Borders
    ws.cell( r(mRow), c(22), r(mRow + 13), c(26), (data[name].remark) )
    .string((data[name].remark) ? data[name].remark : '' )
    .style( styles.centerAlign );  // Remark
    
    if ( data[name].before_img || data[name].after_img || data[name].remark ) {
      
      ws.cell( r(mRow), c(1) ).number( i + 1 ).style( styles.centerAlign );  // Numbering
      ws.cell( r(mRow + 13), c(1), r(mRow + 13), c(26) ).style( styles.blueB );  // Bottom
      mRow += 14;
    }

  });

  // Borders  
  ws.cell( r(currRow), c(1), r(mRow -1), c(1) ).style( styles.blueR );  // Numbering
  ws.cell( r(currRow), c(11), r(mRow - 1), c(11) ).style( styles.blueR );  // Before
  ws.cell( r(currRow), c(21), r(mRow - 1), c(21) ).style( styles.blueR );  // After
  ws.cell( r(currRow - 1), c(1), r(mRow - 1), c(1) ).style( styles.blueLB );  // Outer medium left border
  ws.cell( r(currRow - 1), c(26), r(mRow - 1), c(26) ).style( styles.blueRB );  // Outer medium right border
  ws.cell( r(mRow), c(1), r(mRow - 1), c(26) ).style( styles.blueBB );  // Outer medium bottom border
  
  // # Footer
  // Note
  currRow = mRow + 1;
  ws.cell( r(currRow - 1), c(1), r(currRow - 1), c(26) ).style( styles.blueBB );  // Outer medium bottom border
  ws.cell( r(currRow), c(1), r(currRow + 3), c(1) ).style( styles.blueLB );  // Outer medium left border
  ws.cell( r(currRow), c(26), r(currRow + 3), c(26) ).style( styles.blueRB );  // Outer medium right border
  ws.cell( r(currRow + 3), c(1), r(currRow + 3), c(26) ).style( styles.blueBB );  // Outer medium bottom border
  ws.cell( r(currRow), c(2) ).string('Note :').style({
    font: {
      bold: true,
      italics: true,
      underline: true
    }
  });

  // Footer Quote
  ws.cell( r(currRow + 4), c(1), r(currRow + 5), c(26), true ).string('PREVENTIVE IS BETTER THAN REPAIR').style({
    alignment: {
      vertical: 'center',
      horizontal: 'center'
    },
    font: {
      bold: true,
      size: 14
    }
  });

  wb.write( appRoot + '/dist/reports/' + datas.date.format('YYYYMMDD_') + datas.machine_id + '.xlsx' );
}

router.get( '/', ( req, res, next ) => {

  var datas = {
    date: moment(),
    machine_name: 'FUNCTION LCIA V2.0G',
    machine_id: 'Q230-093332-424',
    executor: 'Nugroho SP',
    line: 'A_BCELL_1004',
    items: {
      'Check motor servo': {
        remark: 'servo motor',
        before_img: '/dist/storage/images/1515057947276_1043.jpg'
      },
      'Check sensor': {
        remark: 'sensor'
      },
      'Check vacuum filter': {
        remark: 'filter vacuum'
      },
      'Check selang angin.': {
      },
      'Check Screw feeder': {
      },
      'Check spead control angin': {
      },
      'Check V-belt': {
      },
      'Check chuk holder': {
      },
      'Check bearing': {
      },
      'Check kebersihan': {
      }
    },
    tools: [
      'WD 40',
      'Majun',
      'Swap',
      'Hex Key set',
      'Combination Pliers',
      'Screw Driver Phillips & Slotted',
      'Vacuum Cleaner'
    ]
  };

  makePmReport( datas );
});

module.exports = router;