// PM Head routes
// --irfan

var Promise = require('bluebird');
var Router = require('restify-router').Router;  // Restify router modules
var moment = require('moment');  // Date Time Modules
var validate = require("validate.js");  // Data validator

var userbank = require( appRoot + '/lib/userbank' );
var db = require( appRoot + '/lib/db' );  // Load knex sql builder

const router = new Router();  // Create new router

// Check new PM Head
function checkNewPMHead( new_pm_head ) {

  return function () {

    return new Promise ( ( resolve, reject ) => {

      var rules = {

        name: {
          presence: {
            allowEmpty: false
          },
          length: {
            maximum: 35
          }
        },
        item: {
          presence: {
            allowEmpty: false
          },
          length: {
            maximum: 500
          }
        },
        cycle: {
          presence: {
            allowEmpty: false
          },
          length: {
            maximum: 15
          }
        },
        mactype_id: {
          presence: {
            allowEmpty: false
          },
          numericality: {
            onlyInteger: true
          }
        },
        std_value: {
          length: {
            maximum: 16
          }
        },
        low_value: {
          length: {
            maximum: 8
          }
        },
        hi_value: {
          length: {
            maximum: 8
          }
        },
        created_by: {
          numericality: {
            onlyInteger: true
          }
        },
        updated_by: {
          numericality: {
            onlyInteger: true
          }
        }

      };

      var err = validate( new_pm_head, rules);

      if ( err ) {
        reject( err );
        return;
      }

      resolve( new_pm_head );

    });  // Promise
  }  // function

};

// Check mod PM Head
function checkModPMHead( mod_pm_head ) {

  return function () {

    return new Promise ( ( resolve, reject ) => {

      var rules = {

        name: {
          length: {
            maximum: 35
          }
        },
        item: {
          length: {
            maximum: 500
          }
        },
        cycle: {
          length: {
            maximum: 15
          }
        },
        mactype_id: {
          numericality: {
            onlyInteger: true
          }
        },
        std_value: {
          length: {
            maximum: 16
          }
        },
        low_value: {
          length: {
            maximum: 8
          }
        },
        hi_value: {
          length: {
            maximum: 8
          }
        },
        updated_by: {
          numericality: {
            onlyInteger: true
          }
        }

      };

      var err = validate( mod_pm_head, rules);

      if ( err ) {
        reject( err );
        return;
      }

      resolve( mod_pm_head );

    });  // Promise
  }  // function

};

// List all PM heads
router.get('/', ( req, res, next ) => {

  var cookies = req.cookies;

  var username = cookies.username;
  var login_token = cookies.login_token;

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  userbank.token( username, login_token )
  .then( userbank.checkPermission_g( 'pm_heads_list' ) )

  .then( function() {

    return db( 'pm_heads as ph' )
    .innerJoin( 'mactypes as mt', 'ph.mactype_id', 'mt.id' )

    .whereNull( 'ph.deleted_at' )
    .select([
      'ph.id as id',
      'ph.name as name',
      'ph.item as item',
      'ph.cycle as cycle',
      'ph.std_value as std_value',
      'ph.low_value as low_value',
      'ph.hi_value as hi_value',
      'ph.mactype_id as mactype_id',
      'mt.name as mactype',
      'ph.created_at as created_at',
      'ph.updated_at as updated_at'
    ]);
  })
  .then( function( rows ) {

    res.send({ success: true, data: rows });
    return next();
  })
  .catch( function( err ) {

    res.send({ success: false, msg: err });
    return next();
  });

});

// Create new PM head
router.post( '/', ( req, res, next ) => {

  var cookies = req.cookies;

  var username = cookies.username;
  var login_token = cookies.login_token;
  var new_pm_head = req.body;

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  userbank.token( username, login_token )
  .then( function(user) {
    new_pm_head.created_by = user.id;
    new_pm_head.updated_by = user.id;

    return user;
  })
  .then( userbank.checkPermission_g( 'pm_heads_manage' ) )
  .then( checkNewPMHead( new_pm_head ) )  // Check new PM vars
  .then( function( data ) {

    return db( 'pm_heads' )
    .returning( '*' )
    .insert( data );

  })
  .then( function( result ) {

    res.send({ success: true, pm_head: result });
    return next();
  })
  .catch( function( err ) {

    res.send({ success: false, msg: err });
    return next();
  });

});

// Update PM Head
router.patch( '/', ( req, res, next ) => {

  var cookies = req.cookies;

  var username = cookies.username;
  var login_token = cookies.login_token;
  var mod_pm_head = req.body;

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  userbank.token( username, login_token )
  .then( function(user) {
    mod_pm_head.updated_by = user.id;
    mod_pm_head.updated_at = moment();

    return user;
  })
  .then( userbank.checkPermission_g( 'smfm_heads_manage' ) )
  .then( checkModPMHead( mod_pm_head ) )  // Check PM Head vars
  .then( function( data ) {
    var id = data.id;
    delete data.id;

    return db( 'pm_heads' )
    .where( 'id', id )
    .returning( '*' )
    .update( data );
  })
  .then( function( result ) {

    res.send({ success: true, smfm_head: result });
    return next();
  })
  .catch( function( err ) {

    res.send({ success: false, msg: err });
    return next();
  });

});

// Delete PM Head
router.del( '/', ( req, res, next ) => {

  var cookies = req.cookies;

  var username = cookies.username;
  var login_token = cookies.login_token;
  var pm_head_id = req.body.id;
  var pm_head = {};

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  userbank.token( username, login_token )
  .then( function(user) {
    pm_head.updated_by = user.id;
    pm_head.updated_at = moment();
    pm_head.deleted_at = moment();

    return user;
  })
  .then( userbank.checkPermission_g( 'pm_heads_manage' ) )
  .then( function() {

    var pm_heads = db( 'pm_heads' );
    if ( typeof pm_head_id === 'object' ) {
      pm_heads = pm_heads.whereIn( 'id', pm_head_id )
    } else {
      pm_heads = pm_heads.where( 'id', pm_head_id )
    }

    return pm_heads.returning( '*' )
    .update( pm_head );
  })
  .then( function( result ) {

    res.send({ success: true, pm_head: result });
    return next();
  })
  .catch( function( err ) {

    res.send({ success: false, msg: err });
    return next();
  });

});

module.exports = router;
