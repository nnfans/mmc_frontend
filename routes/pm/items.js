// PM Item routes
// --irfan

var Promise = require('bluebird');
var Router = require('restify-router').Router;  // Restify router modules
var moment = require('moment');  // Date Time Modules
var validate = require("validate.js");  // Data validator

var userbank = require( appRoot + '/lib/userbank' );
var imgsave = require( appRoot + '/lib/imgsave' );  // Load imgsave library
var excels = require( appRoot + '/lib/excels.js' );  // Load excel reports library
var db = require( appRoot + '/lib/db' );  // Load knex sql builder

const router = new Router();  // Create new router

// Check if PM is complete
function isPMCompleted ( cycle, machine_id ) {
  return db( 'v_pm_current_remain' )
  .where( 'cycle', cycle )
  .where( 'machine_id', machine_id )
  .select( 'pm_head_id' )
  .limit( 1 )
  .then( function( rows ) {
    if ( rows ) {
      if ( rows[0] ) {
        return false;
      }
    }
    return true;
  });
}

// Create PM Reports
function createPmReports ( datas ) {

  var date;
  var items;

  return db( 'v_pm_current_fill' )
  .where( 'machine_id', datas.machine_id )
  .where( 'cycle', datas.cycle)
  .select( '*' )
  .then( function( rows ) {
    if ( rows ) {
      date = rows.map( function( item ) {
        return moment( item.start_at ).format('YYYY-MM-DD')
      }).reduce( function( prev, curr ) {
        if ( moment(prev).isBefore( moment(curr) ) ) {
          return curr;
        }
        return prev;
      }, '2017-01-01');

      items = {};

      rows.forEach( function( item ) {
        items[item.name] = {
          remark: item.remark,
          before_img: item.before_img,
          after_img: item.after_img
        };
      });

      return true;
    }
    return false;
  })
  .then( function ( isOk ) {
    if ( isOk ){
      return excels.makePmReport({
        date,
        cycle: datas.cycle,
        machine_name: datas.machine_name,
        barcode: datas.barcode,
        line: datas.line,
        items: items
      });
    }
    return false;
  })
  .then( function ( file ) {
    if ( file ) {
      return db( 'pm_reports' )
      .insert({
        last_date: date,
        file,
        cycle: datas.cycle,
        barcode: datas.barcode,
        machine_name: datas.machine_name,
        created_by: datas.user_id
      })
      .then( function() {
        return file;
      });
    }
    return false;
  });
}

// List all pms
router.get('/', ( req, res, next ) => {

  var cookies = req.cookies;

  var username = cookies.username;
  var login_token = cookies.login_token;

  var dateFilter = req.query.date_filter;

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  userbank.token( username, login_token )
  .then( userbank.checkPermission_g( 'pm_items_list' ) )

  .then( function() {

    var pm_items = db( 'v_pms' )

    if ( typeof dateFilter === 'string' ) {

      dateFilter = dateFilter.split(' - ');

      dateFilter = dateFilter.map( function ( item ) {  // Subtract 7 hour for GMES date
        return moment( item )
        .add( 7, 'hours' )
        .format('YYYY-MM-DD HH:mm:ss');
      });

    } else {

      dateFilter = [

        moment().startOf( 'day' )
        .add( 7, 'hours' )
        .format('YYYY-MM-DD HH:mm:ss'),

        moment().startOf( 'day' )
        .add( 1, 'days')
        .add( 7, 'hours' )
        .format('YYYY-MM-DD HH:mm:ss')
      ];
    }

    pm_items = pm_items.where(function() {
      this.whereBetween( 'start_at', dateFilter )
      .orWhereBetween( 'start_at', dateFilter )
    });

    return pm_items.select( '*' );

  })
  .then( function( rows ) {

    res.send({ success: true, data: rows });
    return next();
  })
  .catch( function( err ) {

    res.send({ success: false, msg: err });
    return next();
  });

});

// Get current
router.get ( '/current', function ( req, res, next ) {

  var cookies = req.cookies;

  var username = cookies.username;
  var login_token = cookies.login_token;

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  userbank.token( username, login_token )
  .then( userbank.checkPermission_g( 'pm_items_list' ) )

  .then( function() {

    return db( 'v_pm_current' )
    .select( '*' );

  })
  .then( function( rows ) {

    res.send({ success: true, data: rows });
    return next();
  })
  .catch( function( err ) {

    res.send({ success: false, msg: err });
    return next();
  });

});

// Get report list
router.get ( '/reports', function ( req, res, next ) {
  var cookies = req.cookies;
  
  var username = cookies.username;
  var login_token = cookies.login_token;

  var barcode = req.query.barcode;

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  userbank.token( username, login_token )

  .then( function() {

    return db( 'pm_reports' )
    .where('barcode', barcode)
    .select(['last_date', 'file', 'cycle']);

  })
  .then( function( rows ) {

    res.send({ success: true, data: rows });
    return next();
  })
  .catch( function( err ) {

    res.send({ success: false, msg: err });
    return next();
  });
});

// Get remain
router.get ( '/current/remain', function ( req, res, next ) {

  var cookies = req.cookies;

  var username = cookies.username;
  var login_token = cookies.login_token;

  var barcode = req.query.barcode;

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  userbank.token( username, login_token )
  .then( userbank.checkPermission_g( 'pm_items_list' ) )

  .then( function() {

    return db( 'v_pm_current_remain' )
    .where('barcode', barcode)
    .select( '*' );

  })
  .then( function( rows ) {

    res.send({ success: true, data: rows });
    return next();
  })
  .catch( function( err ) {

    res.send({ success: false, msg: err });
    return next();
  });

});

// Submit new pm items
router.post( '/', ( req, res, next ) => {

  var cookies = req.cookies;

  var username = cookies.username;
  var login_token = cookies.login_token;
  var user;

  var barcode = req.body.barcode;
  var data = req.body.data;
  var cycle = req.body.cycle;
  var start_at = req.body.start_at;
  var finish_at = req.body.finish_at;
  var location_id = req.body.location_id;
  var location_name = '';
  var line_changes = false;
  var machine_id;

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  userbank.token( username, login_token )
  .then( u => {
    user = u;
    return u;
  })
  .then( userbank.checkPermission_g( 'pm_items_manage' ) )
  .then( function() {
    // Get Location name from db

    return db( 'locations' )
    .where( 'id', location_id )
    .select( 'name' )
    .limit( 1 );

  })
  .then( function( r ) {

    location_name = r[0].name;  // Assign location name

    // Get last location change histories
    return db( 'location_change_histories' )
    .where( 'machine_code', barcode )
    .select( 'location' )
    .orderBy( 'created_at', 'desc' )
    .limit( 1 );

  })
  .then( function ( r ) {

    // Check last location is changes
    if ( r ) {
      if ( r[0] ) {
        if ( r[0].location != location_name ) {
          line_changes = true;
        }
      } else {
        line_changes = true;
      }
    } else {
      line_changes = true;
    }
  })
  .then( function() {

    if ( line_changes  ) {

      return db( 'location_change_histories' )
      .insert({
        created_by: user.id,
        location: location_name,
        remark: 'PM',
        machine_code: barcode
      });
    }

  })
  .then( function() {

    return db( 'machines' )
    .where( 'barcode', barcode )
    .limit( 1 )
    .returning('id')
    .update( {
      'location_id': location_id,
      'updated_by': user.id,
      'updated_at': moment()
    });

  })
  .then( function ( m_id ) {
    var datas = [];
    machine_id = m_id[0];
    data.forEach( function ( item ) {

      if ( item.before_img ) {
        item.before_img = imgsave.quickSaveBase64( item.before_img );
      } else {
        item.before_img = null;
      }

      if ( item.after_img ) {
        item.after_img = imgsave.quickSaveBase64( item.after_img );
      } else {
        item.after_img = null;
      }

      datas.push({
        pm_head_id: item.id,
        machine_id: m_id[0],
        status: item.status,
        value: item.value,
        remark: item.remark,
        start_at: item.start_at,
        finish_at: item.finish_at,
        created_by: user.id,
        location_id: location_id,
        before_img: item.before_img,
        after_img: item.after_img
      });
    });

    return db( 'pm_items' )
    .returning( 'id' )
    .insert( datas );
  })
  .then( function (pm_item) {
    return isPMCompleted( cycle, machine_id );
  })
  .then( function ( completed ) {
    if ( completed) {
      return db( 'mactypes as mt' )
      .innerJoin('machines as m', 'mt.id', 'm.mactype_id')
      .where( 'm.barcode', barcode )
      .select( 'mt.name' )
      .limit( 1 )
      .then( function( machine_name ) {

        createPmReports({
          cycle,
          barcode,
          machine_id,
          machine_name: machine_name[0].name,
          line: location_name,
          mactype_id: data[0].mactype_id,
          user_id: user.id
        });
      })
    }
  })
  .then( function ( file ) {

    res.send({ success: true, pm_item: file });
    return next();
  });

});

// Update PM item
router.patch( '/', ( req, res, next ) => {
  
    var cookies = req.cookies;
  
    var username = cookies.username;
    var login_token = cookies.login_token;
    var user;
  
    var cycle = req.body.cycle;
    var barcode = req.body.barcode;
    var data = req.body.data;
    var finish_at = req.body.finish_at;
    var location_id = req.body.location_id;
    var location_name = '';
    var line_changes = false;
    var machine_id;
  
    if ( !username || !login_token ) {
      res.send({ success: false });
      return next();
    };
  
    userbank.token( username, login_token )
    .then( u => {
      user = u;
      return u;
    })
    .then( userbank.checkPermission_g( 'pm_items_manage' ) )
    .then( function() {
      // Get Location name from db
  
      return db( 'locations' )
      .where( 'id', location_id )
      .select( 'name' )
      .limit( 1 );
  
    })
    .then( function( r ) {
  
      location_name = r[0].name;  // Assign location name
  
      // Get last location change histories
      return db( 'location_change_histories' )
        .where( 'machine_code', barcode )
        .select( 'location' )
        .orderBy( 'created_at', 'desc' )
        .limit( 1 );
    })
    .then( function ( r ) {

      // Check last location is changes
      if ( r ) {
        if ( r[0] ) {
          if ( r[0].location != location_name ) {
            line_changes = true;
          }
        } else {
          line_changes = true;
        }
      } else {
        line_changes = true;
      }
    })
    .then( function() {
  
      if ( line_changes  ) {
  
        return db( 'location_change_histories' )
        .insert({
          created_by: user.id,
          location: location_name,
          remark: 'PM',
          machine_code: barcode
        });
      }
    })
    .then( function() {
  
      return db( 'machines' )
      .where( 'barcode', barcode )
      .limit( 1 )
      .returning('id')
      .update( {
        'location_id': location_id,
        'updated_by': user.id,
        'updated_at': moment()
      });
    })
    .then( function( m_id ) {
      machine_id = m_id[0];

      return Promise.map( data, item => {

        return db( 'pm_items' )
        .where( 'id', item.item_id )
        .select([
          'status',
          'value',
          'remark',
          'finish_at',
          'before_img',
          'after_img'
        ])
        .then( function ( r ) {

          if ( item.status == r.status ) {
            delete item.status;
          }
          if ( item.value == r.value ) {
            delete item.value;
          }
          if ( item.remark == r.remark ) {
            delete item.remark;
          }
          if ( item.finish_at == r.finish_at ) {
            delete item.finish_at;
          }
          if ( item.before_img == r.before_img ) {
            delete item.before_img;
          } else if ( item.before_img ) {
            item.before_img = imgsave.quickSaveBase64( item.before_img );
          } else {
            delete item.after_img;
          }
          if ( item.after_img == r.after_img ) {
            delete item.after_img;
          } else if ( item.after_img ) {
            item.after_img = imgsave.quickSaveBase64( item.after_img );
          } else {
            delete item.after_img;
          }

          var id = item.item_id;
          delete item.id;
          delete item.item_id;

          if ( Object.keys(item).length ) {

            return db( 'pm_items' )
              .where( 'id', id )
              .returning( 'id' )
              .update( item );
          }
        });

      })
    })
    .then( function (pm_item) {
      return isPMCompleted( cycle, machine_id );
    })
    .then( function ( completed ) {
      if ( completed) {
        return db( 'mactypes as mt' )
        .innerJoin('machines as m', 'mt.id', 'm.mactype_id')
        .where( 'm.barcode', barcode )
        .select( 'mt.name' )
        .limit( 1 )
        .then( function( machine_name ) {
  
          createPmReports({
            cycle,
            barcode,
            machine_id,
            machine_name: machine_name[0].name,
            line: location_name,
            mactype_id: data[0].mactype_id,
            user_id: user.id
          });
        })
      }
    })
    .then( function ( file ) {
  
      res.send({ success: true, pm_item: file });
      return next();
    });
  
  });

// Delete PM Item
router.del( '/', ( req, res, next ) => {

  var cookies = req.cookies;

  var username = cookies.username;
  var login_token = cookies.login_token;
  var pm_item_id = req.body.id;
  var pm_item = {};

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  userbank.token( username, login_token )
  .then( function(user) {
    pm_item.updated_by = user.id;
    pm_item.updated_at = moment();
    pm_item.deleted_at = moment();

    return user;
  })
  .then( userbank.checkPermission_g( 'pm_items_manage' ) )
  .then( function() {

    return db( 'pm_items' )
    .where( 'id', pm_item_id )
    .returning( '*' )
    .update( pm_item );
  })
  .then( function( result ) {

    res.send({ success: true, pm_item: result });
    return next();
  })
  .catch( function( err ) {

    res.send({ success: false, msg: err });
    return next();
  });

});

module.exports = router;
