// mactype routes
// --irfan

var Promise = require('bluebird');
var Router = require('restify-router').Router;  // Restify router modules
var moment = require('moment');  // Date Time Modules
var validate = require("validate.js");  // Data validator

var userbank = require( appRoot + '/lib/userbank' );
var db = require( appRoot + '/lib/db' );  // Load knex sql builder

const router = new Router();  // Create new router

// Check Mactype name
function checkName ( name ) {

  return new Promise ( ( resolve, reject ) => {

    if ( name ) {

      db( 'mactypes' )
      .where( 'name', name )
      .select( 'id' )
      .limit(1)
      .asCallback( function ( err, rows ){

        if ( err ) {
          reject( err );
          return;
        };

        if ( rows[0] ) {

          reject( {
            name: [
              'Name has been exists.'
            ]
          });
          return;
        } else {
          resolve();
        };

      });  // Callback

    } else {
      resolve();
    }

  });  // Promise

};

// Check new Machine
function checkNewMactype( new_mactype ) {

  return function () {

    return new Promise ( ( resolve, reject ) => {

      var rules = {

        name: {
          presence: {
            allowEmpty: false
          },
          length: {
            maximum: 20
          }
        },
        type: {
          presence: {
            allowEmpty: false
          },
          length: {
            maximum: 6
          }
        },
        spec: {
          length: {
            maximum: 80
          }
        },
        ver: {
          length: {
            maximum: 20
          }
        },
        created_by: {
          numericality: {
            onlyInteger: true
          }
        },
        updated_by: {
          numericality: {
            onlyInteger: true
          }
        }

      };

      var err = validate( new_mactype, rules);

      if ( err ) {
        reject( err );
        return;
      }

      checkName( new_mactype.name )
      .then( function () {
        resolve( new_mactype );
      })
      .catch( function ( err ) {
        reject( err );
      });

    });  // Promise
  }  // function

};

// Check mod Mactype
function checkModMactype( mod_mactype ) {

  return function () {

    return new Promise ( ( resolve, reject ) => {

      var rules = {

        name: {
          length: {
            maximum: 20
          }
        },
        type: {
          length: {
            maximum: 6
          }
        },
        spec: {
          length: {
            maximum: 80
          }
        },
        ver: {
          length: {
            maximum: 20
          }
        },
        created_by: {
          numericality: {
            onlyInteger: true
          }
        },
        updated_by: {
          numericality: {
            onlyInteger: true
          }
        }

      };

      var err = validate( mod_mactype, rules);

      if ( err ) {
        reject( err );
        return;
      }

      checkName( mod_mactype.name )
      .then( function () {
        resolve( mod_mactype );
      })
      .catch( function ( err ) {
        reject( err );
      });

    });  // Promise
  }  // function

};

// List all mactypes
router.get('/', ( req, res, next ) => {

  var cookies = req.cookies;

  var username = cookies.username;
  var login_token = cookies.login_token;

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  userbank.token( username, login_token )
  .then( userbank.checkPermission_g( 'mactypes_list' ) )

  .then( function() {

    return db( 'mactypes' )
    .whereNull( 'deleted_at' )
    .select( '*' );
  })
  .then( function( rows ) {

    res.send({ success: true, data: rows });
    return next();
  })
  .catch( function( err ) {

    res.send({ success: false, msg: err });
    return next();
  });

});

// Selection for mactypes
router.get('/selection', ( req, res, next ) => {

  var cookies = req.cookies;

  var username = cookies.username;
  var login_token = cookies.login_token;

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  db( 'mactypes' )
  .whereNull( 'deleted_at' )
  .select([
    'id as value',
    'name as text'
  ])
  .orderBy('name', 'asc')
  .then( function( rows ) {

    res.send({ success: true, data: rows });
    return next();
  })
  .catch( function( err ) {

    res.send({ success: false, msg: err });
    return next();
  });

});

// Create new mactypes
router.post( '/', ( req, res, next ) => {

  var cookies = req.cookies;

  var username = cookies.username;
  var login_token = cookies.login_token;
  var new_mactype = req.body;

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  userbank.token( username, login_token )
  .then( function(user) {
    new_mactype.created_by = user.id;
    new_mactype.updated_by = user.id;

    return user;
  })
  .then( userbank.checkPermission_g( 'mactypes_create' ) )
  .then( checkNewMactype( new_mactype ) )  // Check mactype vars
  .then( function( data ) {

    return db( 'mactypes' )
    .returning( '*' )
    .insert( data );

  })
  .then( function( mactype ) {

    res.send({ success: true, mactype: mactype });
    return next();
  })
  .catch( function( err ) {

    res.send({ success: false, msg: err });
    return next();
  });

});

// Update mactypes
router.patch( '/', ( req, res, next ) => {

  var cookies = req.cookies;

  var username = cookies.username;
  var login_token = cookies.login_token;
  var mod_mactype = req.body.data;

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  userbank.token( username, login_token )
  .then( function(user) {
    mod_mactype.updated_by = user.id;
    mod_mactype.updated_at = moment();

    return user;
  })
  .then( userbank.checkPermission_g( 'mactypes_manage' ) )
  .then( () => mod_mactype)
  .map( function ( item ) {  // Check mactype vars
    return checkModMactype( item )();
  })
  .map( function( data ) {
    var id = data.id;
    delete data.id;

    return db( 'mactypes' )
    .where( 'id', id )
    .returning( '*' )
    .update( data );
  })
  .then ( function( result ) {

    res.send({ success: true, mactype: result });
    return next();
  })
  .catch( function( err ) {

    res.send({ success: false, msg: err });
    return next();
  });

});

// Delete mactypes
router.del( '/', ( req, res, next ) => {

  var cookies = req.cookies;

  var username = cookies.username;
  var login_token = cookies.login_token;
  var mactype_id = req.body.id;
  var mactype = {};

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  userbank.token( username, login_token )
  .then( function(user) {
    mactype.updated_by = user.id;
    mactype.updated_at = moment();
    mactype.deleted_at = moment();

    return user;
  })
  .then( userbank.checkPermission_g( 'mactypes_manage' ) )
  .then( function() {

    var mactypes = db( 'mactypes' );
    if ( typeof mactype_id === 'object' ) {
      mactypes = mactypes.whereIn( 'id', mactype_id )
    } else {
      mactypes = mactypes.where( 'id', mactype_id )
    }

    return mactypes.returning( '*' )
    .update( mactype );
  })
  .then( function( result ) {

    res.send({ success: true, mactype: result });
    return next();
  })
  .catch( function( err ) {

    res.send({ success: false, msg: err });
    return next();
  });

});

module.exports = router;
