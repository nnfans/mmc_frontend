// Result routes
// --irfan

var Router = require('restify-router').Router;  // Restify router modules

const router = new Router();  // Create new router

router.add("/summary", require("./summary"));

module.exports = router;  // Export routes
