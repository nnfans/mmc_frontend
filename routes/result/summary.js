// Result Summary routes
// --irfan

var Promise = require('bluebird');
var Router = require('restify-router').Router;  // Restify router modules
var moment = require('moment');  // Date Time Modules
var validate = require("validate.js");  // Data validator

var userbank = require( appRoot + '/lib/userbank' );
var db = require( appRoot + '/lib/db' );  // Load knex sql builder

const router = new Router();  // Create new router

// Get Summary Result per Machine
router.get('/', ( req, res, next ) => {

  var cookies = req.cookies;

  var username = cookies.username;
  var login_token = cookies.login_token;
  var barcode = req.query.barcode;

  var part_changes;
  var pm_history;
  var movement;

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  userbank.token( username, login_token )
  .then( function() {

    return db( 'v_latest_part_change' )
    .where( 'barcode', barcode )
    .select();
  })
  .then( function( rows ) {

    part_changes = rows;

    return db( 'v_machine_summary_pm' )
    .where( 'barcode', barcode )
    .select();
  })
  .then( function( rows ) {

    pm_history = rows;

    return db( 'location_change_histories' )
    .where( 'machine_code', barcode )
    .select();
  })
  .then( function( rows ) {

    movement = rows;

    res.send({ success: true,
      data: {
        part_changes,
        pm_history,
        movement
      }
    });
    return next();
  })
  .catch( function( err ) {

    res.send({ success: false, msg: err });
    return next();
  });

});

module.exports = router;