// locations routes
// --irfan

var Promise = require('bluebird');
var Router = require('restify-router').Router;  // Restify router modules
var moment = require('moment');  // Date Time Modules
var validate = require("validate.js");  // Data validator

var userbank = require( appRoot + '/lib/userbank' );
var db = require( appRoot + '/lib/db' );  // Load knex sql builder

const router = new Router();  // Create new router

// Check Location Name
function checkName ( name ) {

  return new Promise ( ( resolve, reject ) => {

    if ( name ) {

      db( 'locations' )
      .where( 'name', name )
      .select( 'id' )
      .limit(1)
      .asCallback( function ( err, rows ){

        if ( err ) {
          reject( err );
          return;
        };

        if ( rows[0] ) {

          reject( {
            name: [
              'Name has been exists.'
            ]
          });
          return;
        } else {
          resolve();
        };

      });  // Callback

    } else {
      resolve();
    }

  });  // Promise

};

// Check new Location
function checkNewLocation( new_location ) {

  return function () {

    return new Promise ( ( resolve, reject ) => {

      var rules = {

        name: {
          presence: {
            allowEmpty: false
          },
          length: {
            maximum: 20
          }
        },
        type: {
          presence: {
            allowEmpty: false
          },
          length: {
            maximum: 15
          }
        },
        remark: {
          length: {
            maximum: 30
          }
        },
        created_by: {
          numericality: {
            onlyInteger: true
          }
        },
        updated_by: {
          numericality: {
            onlyInteger: true
          }
        }

      };

      var err = validate( new_location, rules);

      if ( err ) {
        reject( err );
        return;
      }

      checkName( new_location.name )
      .then( function () {
        resolve( new_location );
      })
      .catch( function ( err ) {
        reject( err );
      });

    });  // Promise
  }  // function

};

// Check mod Location
function checkModLocation( mod_location ) {

  return function () {

    return new Promise ( ( resolve, reject ) => {

      var rules = {

        name: {
          length: {
            maximum: 20
          }
        },
        type: {
          length: {
            maximum: 15
          }
        },
        remark: {
          length: {
            maximum: 30
          }
        },
        updated_by: {
          numericality: {
            onlyInteger: true
          }
        }

      };

      var err = validate( mod_location, rules);

      if ( err ) {
        reject( err );
        return;
      }

      checkName( mod_location.name )
      .then( function () {
        resolve( mod_location );
      })
      .catch( function ( err ) {
        reject( err );
      });

    });  // Promise
  }  // function

};

// List all locations
router.get('/', ( req, res, next ) => {

  var cookies = req.cookies;

  var username = cookies.username;
  var login_token = cookies.login_token;

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  userbank.token( username, login_token )
  .then( userbank.checkPermission_g( 'locations_list' ) )

  .then( function() {

    return db( 'locations' )
    .whereNull( 'deleted_at' )
    .select( '*' );
  })
  .then( function( rows ) {

    res.send({ success: true, data: rows });
    return next();
  })
  .catch( function( err ) {

    res.send({ success: false, msg: err });
    return next();
  });

});


// Selection for locations
router.get('/selection', ( req, res, next ) => {

  var cookies = req.cookies;

  var username = cookies.username;
  var login_token = cookies.login_token;

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  db( 'locations' )
  .whereNull( 'deleted_at' )
  .select([
    'id as value',
    'name as text'
  ])
  .orderBy('name', 'asc')
  .then( function( rows ) {

    res.send({ success: true, data: rows });
    return next();
  })
  .catch( function( err ) {

    res.send({ success: false, msg: err });
    return next();
  });

});


// Create new locations
router.post( '/', ( req, res, next ) => {

  var cookies = req.cookies;

  var username = cookies.username;
  var login_token = cookies.login_token;
  var new_location = req.body;

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  userbank.token( username, login_token )
  .then( function(user) {
    new_location.created_by = user.id;
    new_location.updated_by = user.id;

    return user;
  })
  .then( userbank.checkPermission_g( 'locations_manage' ) )
  .then( checkNewLocation( new_location ) )  // Check location vars
  .then( function( data ) {

    return db( 'locations' )
    .returning( '*' )
    .insert( data );

  })
  .then( function( location ) {

    res.send({ success: true, location: location });
    return next();
  })
  .catch( function( err ) {

    res.send({ success: false, msg: err });
    return next();
  });

});

// Update location
router.patch( '/', ( req, res, next ) => {

  var cookies = req.cookies;

  var username = cookies.username;
  var login_token = cookies.login_token;
  var mod_location = req.body;

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  userbank.token( username, login_token )
  .then( function(user) {
    mod_location.updated_by = user.id;
    mod_location.updated_at = moment();

    return user;
  })
  .then( userbank.checkPermission_g( 'locations_manage' ) )
  .then( checkModLocation( mod_location ) )  // Check location vars
  .then( function( data ) {
    var id = data.id;
    delete data.id;

    return db( 'locations' )
    .where( 'id', id )
    .returning( '*' )
    .update( data );
  })
  .then( function( result ) {

    res.send({ success: true, location: result });
    return next();
  })
  .catch( function( err ) {

    res.send({ success: false, msg: err });
    return next();
  });

});

// Delete location
router.del( '/', ( req, res, next ) => {

  var cookies = req.cookies;

  var username = cookies.username;
  var login_token = cookies.login_token;
  var location_id = req.body.id;
  var location = {};

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  userbank.token( username, login_token )
  .then( function(user) {
    location.updated_by = user.id;
    location.updated_at = moment();
    location.deleted_at = moment();

    return user;
  })
  .then( userbank.checkPermission_g( 'locations_manage' ) )
  .then( function() {

    var locations = db( 'locations' );
    if ( typeof location_id === 'object' ) {
      locations = locations.whereIn( 'id', location_id )
    } else {
      locations = locations.where( 'id', location_id )
    }

    return locations.returning( '*' )
    .update( location );
  })
  .then( function( result ) {

    res.send({ success: true, location: result });
    return next();
  })
  .catch( function( err ) {

    res.send({ success: false, msg: err });
    return next();
  });

});

module.exports = router;
