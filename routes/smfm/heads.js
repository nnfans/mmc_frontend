// S-MFM Head routes
// --irfan

var Promise = require('bluebird');
var Router = require('restify-router').Router;  // Restify router modules
var moment = require('moment');  // Date Time Modules
var validate = require("validate.js");  // Data validator

var userbank = require( appRoot + '/lib/userbank' );
var db = require( appRoot + '/lib/db' );  // Load knex sql builder

const router = new Router();  // Create new router

// Check new S-MFM Head
function checkNewSmfmHead( new_smfm_head ) {

  return function () {

    return new Promise ( ( resolve, reject ) => {

      var rules = {

        name: {
          presence: {
            allowEmpty: false
          },
          length: {
            maximum: 35
          }
        },
        item: {
          presence: {
            allowEmpty: false
          },
          length: {
            maximum: 500
          }
        },
        mactype_id: {
          presence: {
            allowEmpty: false
          },
          numericality: {
            onlyInteger: true
          }
        },
        std_value: {
          length: {
            maximum: 16
          }
        },
        low_value: {
          length: {
            maximum: 8
          }
        },
        hi_value: {
          length: {
            maximum: 8
          }
        },
        created_by: {
          numericality: {
            onlyInteger: true
          }
        },
        updated_by: {
          numericality: {
            onlyInteger: true
          }
        }

      };

      var err = validate( new_smfm_head, rules);

      if ( err ) {
        reject( err );
        return;
      }

      resolve( new_smfm_head );

    });  // Promise
  }  // function

};

// Check mod S-MFM Head
function checkModSmfmHead( mod_smfm_head ) {

  return function () {

    return new Promise ( ( resolve, reject ) => {

      var rules = {

        name: {
          length: {
            maximum: 35
          }
        },
        item: {
          length: {
            maximum: 500
          }
        },
        mactype_id: {
          numericality: {
            onlyInteger: true
          }
        },
        std_value: {
          length: {
            maximum: 16
          }
        },
        low_value: {
          length: {
            maximum: 8
          }
        },
        hi_value: {
          length: {
            maximum: 8
          }
        },
        updated_by: {
          numericality: {
            onlyInteger: true
          }
        }

      };

      var err = validate( mod_smfm_head, rules);

      if ( err ) {
        reject( err );
        return;
      }

      resolve( mod_smfm_head );

    });  // Promise
  }  // function

};

// List all smfm heads
router.get('/', ( req, res, next ) => {

  var cookies = req.cookies;

  var username = cookies.username;
  var login_token = cookies.login_token;

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  userbank.token( username, login_token )
  .then( userbank.checkPermission_g( 'smfm_heads_list' ) )

  .then( function() {

    return db( 'smfm_heads as sh' )
    .innerJoin( 'mactypes as mt', 'sh.mactype_id', 'mt.id' )

    .whereNull( 'sh.deleted_at' )
    .whereNull( 'mt.deleted_at' )
    .select([
      'sh.id as id',
      'sh.name as name',
      'sh.item as item',
      'sh.std_value as std_value',
      'sh.low_value as low_value',
      'sh.hi_value as hi_value',
      'sh.mactype_id as mactype_id',
      'mt.name as mactype',
      'sh.created_at as created_at',
      'sh.updated_at as updated_at'
    ]);
  })
  .then( function( rows ) {

    res.send({ success: true, data: rows });
    return next();
  })
  .catch( function( err ) {

    res.send({ success: false, msg: err });
    return next();
  });

});

// Create new smfm head
router.post( '/', ( req, res, next ) => {

  var cookies = req.cookies;

  var username = cookies.username;
  var login_token = cookies.login_token;
  var new_smfm_head = req.body;

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  userbank.token( username, login_token )
  .then( function(user) {
    new_smfm_head.created_by = user.id;
    new_smfm_head.updated_by = user.id;

    return user;
  })
  .then( userbank.checkPermission_g( 'smfm_heads_manage' ) )
  .then( checkNewSmfmHead( new_smfm_head ) )  // Check location vars
  .then( function( data ) {

    return db( 'smfm_heads' )
    .returning( '*' )
    .insert( data );

  })
  .then( function( smfm_head ) {

    res.send({ success: true, smfm_head: smfm_head });
    return next();
  })
  .catch( function( err ) {

    res.send({ success: false, msg: err });
    return next();
  });

});

// Update smfm head
router.patch( '/', ( req, res, next ) => {

  var cookies = req.cookies;

  var username = cookies.username;
  var login_token = cookies.login_token;
  var mod_smfm_head = req.body;

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  userbank.token( username, login_token )
  .then( function(user) {
    mod_smfm_head.updated_by = user.id;
    mod_smfm_head.updated_at = moment();

    return user;
  })
  .then( userbank.checkPermission_g( 'smfm_heads_manage' ) )
  .then( checkModSmfmHead( mod_smfm_head ) )  // Check location vars
  .then( function( data ) {
    var id = data.id;
    delete data.id;

    return db( 'smfm_heads' )
    .where( 'id', id )
    .returning( '*' )
    .update( data );
  })
  .then( function( result ) {

    res.send({ success: true, smfm_head: result });
    return next();
  })
  .catch( function( err ) {

    res.send({ success: false, msg: err });
    return next();
  });

});

// Delete SMFM Head
router.del( '/', ( req, res, next ) => {

  var cookies = req.cookies;

  var username = cookies.username;
  var login_token = cookies.login_token;
  var smfm_head_id = req.body.id;
  var smfm_head = {};

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  userbank.token( username, login_token )
  .then( function(user) {
    smfm_head.updated_by = user.id;
    smfm_head.updated_at = moment();
    smfm_head.deleted_at = moment();

    return user;
  })
  .then( userbank.checkPermission_g( 'smfm_heads_manage' ) )
  .then( function() {

    var smfm_heads = db( 'smfm_heads' );
    if ( typeof smfm_head_id === 'object' ) {
      smfm_heads = smfm_heads.whereIn( 'id', smfm_head_id )
    } else {
      smfm_heads = smfm_heads.where( 'id', smfm_head_id )
    }

    return smfm_heads.returning( '*' )
    .update( smfm_head );
  })
  .then( function( result ) {

    res.send({ success: true, smfm_head: result });
    return next();
  })
  .catch( function( err ) {

    res.send({ success: false, msg: err });
    return next();
  });

});

module.exports = router;
