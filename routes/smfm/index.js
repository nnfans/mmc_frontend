// S-MFM routes
// --irfan

var Router = require('restify-router').Router;  // Restify router modules

const router = new Router();  // Create new router

router.add("/heads", require("./heads"));
router.add("/items", require("./items"));

module.exports = router;  // Export routes
