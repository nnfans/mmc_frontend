// S-MFM Item routes
// --irfan

var Promise = require('bluebird');
var Router = require('restify-router').Router;  // Restify router modules
var moment = require('moment');  // Date Time Modules
var validate = require("validate.js");  // Data validator

var userbank = require( appRoot + '/lib/userbank' );
var db = require( appRoot + '/lib/db' );  // Load knex sql builder

const router = new Router();  // Create new router

function createBetween ( interval ) {
  return [

    moment( interval )
    .subtract( 7, 'hours' )
    .startOf( 'day' )
    .add( 7, 'hours' )
    .format('YYYY-MM-DD HH:mm:ss'),

    moment( interval )
    .subtract( 7, 'hours' )
    .startOf( 'day' )
    .add( 1, 'days')
    .add( 7, 'hours' )
    .format('YYYY-MM-DD HH:mm:ss')

  ];
}

function notExistsStart( machine_id, smfm_head_id, start_at ) {

  return new Promise ( ( resolve, reject ) => {

    db( 'smfm_items' )
    .where( 'machine_id', machine_id )
    .where( 'smfm_head_id', smfm_head_id )
    .whereBetween( 'start_at', createBetween( start_at ) )
    .select( 'id' )
    .limit( 1 )
    .asCallback( function ( err, rows ){

      if ( err ) {
        reject( err );
        return;
      };

      if ( rows[0] ) {

        reject( {
          start_at: [
            'Already checked.'
          ]
        });
        return;
      } else {
        resolve();
      };

    });  // Callback

  });  // Promise

}

// Check new Smfm Item
function checkNewSmfmItem( new_smfm_item ) {

  return function () {

    return new Promise ( ( resolve, reject ) => {

      var rules = {

        smfm_head_id: {
          presence: {
            allowEmpty: false
          },
          numericality: {
            onlyInteger: true
          }
        },
        machine_id: {
          presence: {
            allowEmpty: false
          },
          numericality: {
            onlyInteger: true
          }
        },
        value: {
          presence: {
            allowEmpty: false
          },
          length: {
            maximum: 8
          }
        },
        start_at: {
          presence: {
            allowEmpty: false
          },
          length: {
            is: 19
          }
        },
        finish_at: {
          length: {
            is: 19
          }
        },
        created_by: {
          numericality: {
            onlyInteger: true
          }
        }

      };

      var err = validate( new_smfm_item, rules);

      if ( err ) {
        reject( err );
        return;
      }

      notExistsStart(
        new_smfm_item.machine_id,
        new_smfm_item.smfm_head_id,
        new_smfm_item.start_at
      )
      .then( function () {
        resolve( new_smfm_item );
      })
      .catch( function ( err ) {
        reject( err );
      });

    });  // Promise
  }  // function

};

// List all smfm
router.get('/', ( req, res, next ) => {

  var cookies = req.cookies;

  var username = cookies.username;
  var login_token = cookies.login_token;

  var dateFilter = req.query.date_filter;

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  userbank.token( username, login_token )
  .then( userbank.checkPermission_g( 'smfm_items_list' ) )

  .then( function() {

    var smfm_items = db( 'v_smfms' )

    if ( typeof dateFilter === 'string' ) {

      dateFilter = dateFilter.split(' - ');

      dateFilter = dateFilter.map( function ( item ) {  // Subtract 7 hour for GMES date
        return moment( item )
        .add( 7, 'hours' )
        .format('YYYY-MM-DD HH:mm:ss');
      });

    } else {

      dateFilter = [

        moment().startOf( 'day' )
        .add( 7, 'hours' )
        .format('YYYY-MM-DD HH:mm:ss'),

        moment().startOf( 'day' )
        .add( 1, 'days')
        .add( 7, 'hours' )
        .format('YYYY-MM-DD HH:mm:ss')
      ];
    }

    smfm_items = smfm_items.where(function() {
      this.whereBetween( 'start_at', dateFilter )
      .orWhereBetween( 'start_at', dateFilter )
    });

    return smfm_items.select( '*' );

  })
  .then( function( rows ) {

    res.send({ success: true, data: rows });
    return next();
  })
  .catch( function( err ) {

    res.send({ success: false, msg: err });
    return next();
  });

});

// Get current
router.get ( '/today', function ( req, res, next ) {

  var cookies = req.cookies;

  var username = cookies.username;
  var login_token = cookies.login_token;

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  userbank.token( username, login_token )
  .then( userbank.checkPermission_g( 'smfm_items_list' ) )

  .then( function() {

    return db( 'v_smfm_today' )
    .select( '*' );

  })
  .then( function( rows ) {

    res.send({ success: true, data: rows });
    return next();
  })
  .catch( function( err ) {

    res.send({ success: false, msg: err });
    return next();
  });

});

// Get remain
router.get ( '/today/remain', function ( req, res, next ) {

  var cookies = req.cookies;

  var username = cookies.username;
  var login_token = cookies.login_token;

  var barcode = req.query.barcode;

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  userbank.token( username, login_token )
  .then( userbank.checkPermission_g( 'smfm_items_list' ) )

  .then( function() {

    return db( 'v_smfm_today_remain' )
    .where('barcode', barcode)
    .select( '*' );

  })
  .then( function( rows ) {

    res.send({ success: true, data: rows });
    return next();
  })
  .catch( function( err ) {

    res.send({ success: false, msg: err });
    return next();
  });

});

router.get ( '/check', function ( req, res, next ) {
  var cookies = req.cookies;

  var username = cookies.username;
  var login_token = cookies.login_token;

  var machine_id = req.query.machine_id;
  var smfm_head_id = req.query.smfm_head_id;
  var time = req.query.time;

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  userbank.token( username, login_token )
  .then( userbank.checkPermission_g( 'smfm_items_list' ) )

  .then( function() {

    return notExistsStart( machine_id, smfm_head_id, time );

  })
  .then( function( result ) {

    res.send({ success: true, result: false, data: 'Not checked' });
    return next();
  })
  .catch( function( err ) {

    res.send({ success: ( ( err.start_at ) ? true : false ), result: ( ( err.start_at ) ? true : false ), msg: err });
    return next();
  });
});

// Create new S-MFM item
router.post( '/', ( req, res, next ) => {

  var cookies = req.cookies;

  var username = cookies.username;
  var login_token = cookies.login_token;
  var user;

  var barcode = req.body.barcode;
  var data = req.body.data;
  var start_at = req.body.start_at;
  var finish_at = req.body.finish_at;
  var location_id = req.body.location_id;
  var location_name = '';
  var line_changes = false;

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  userbank.token( username, login_token )
  .then( u => {
    user = u;
    return u;
  })
  .then( userbank.checkPermission_g( 'smfm_items_manage' ) )
  .then( function() {
    // Get Location name from db

    return db( 'locations' )
    .where( 'id', location_id )
    .select( 'name' )
    .limit( 1 );

  })
  .then( function( r ) {

    location_name = r[0].name;  // Assign location name

    // Get last location change histories
    return db( 'location_change_histories' )
    .where( 'machine_code', barcode )
    .select( 'location' )
    .orderBy( 'created_at', 'desc' )
    .limit( 1 );

  })
  .then( function ( r ) {

    // Check last location is changes
    if ( r ) {
      if ( r[0] ) {
        if ( r[0].location != location_name ) {
          line_changes = true;
        }
      } else {
        line_changes = true;
      }
    } else {
      line_changes = true;
    }
  })
  .then( function() {

    if ( line_changes  ) {

      return db( 'location_change_histories' )
      .insert({
        created_by: user.id,
        location: location_name,
        remark: 'S-MFM',
        machine_code: barcode
      });
    }

  })
  .then( function() {

    return db( 'machines' )
    .where( 'barcode', barcode )
    .limit( 1 )
    .returning('id')
    .update( {
      'location_id': location_id,
      'updated_by': user.id,
      'updated_at': moment()
    });

  })
  .then( function ( machine_id ) {
    var datas = [];
    data.forEach( function ( item ) {
      datas.push({
        smfm_head_id: item.id,
        machine_id: machine_id[0],
        status: item.status,
        value: item.value,
        remark: item.remark,
        start_at: start_at,
        finish_at: finish_at,
        created_by: user.id,
        location_id: location_id
      });
    });

    return db( 'smfm_items' )
    .where( 'barcode', barcode )
    .limit( 1 )
    .returning( 'id' )
    .insert( datas );
  })
  .then( function( smfm_item ) {

    res.send({ success: true, smfm_head: smfm_item });
    return next();
  })
  // .catch( function( err ) {

  //   res.send({ success: false, msg: err });
  //   return next();
  // });

});

// Update S-MFM item
router.patch( '/', ( req, res, next ) => {

  var cookies = req.cookies;

  var username = cookies.username;
  var login_token = cookies.login_token;
  var mod_smfm_item = req.body;

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  userbank.token( username, login_token )
  .then( userbank.checkPermission_g( 'smfm_items_manage' ) )
  .then( checkModSmfmItem( mod_smfm_item ) )  // Check Modified S-MFM vars
  .then( function( data ) {
    var id = data.id;
    delete data.id;

    return db( 'smfm_items' )
    .where( 'id', id )
    .returning( '*' )
    .update( data );
  })
  .then( function( result ) {

    res.send({ success: true, smfm_item: result });
    return next();
  })
  .catch( function( err ) {

    res.send({ success: false, msg: err });
    return next();
  });

});

// Delete S-MFM Item
router.del( '/', ( req, res, next ) => {

  var cookies = req.cookies;

  var username = cookies.username;
  var login_token = cookies.login_token;
  var smfm_item_id = req.body.id;
  var smfm_item = {};

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  userbank.token( username, login_token )
  .then( function(user) {
    smfm_item.updated_by = user.id;
    smfm_item.updated_at = moment();
    smfm_item.deleted_at = moment();

    return user;
  })
  .then( userbank.checkPermission_g( 'smfm_items_manage' ) )
  .then( function() {

    return db( 'smfm_items' )
    .where( 'id', smfm_item_id )
    .returning( '*' )
    .update( smfm_item );
  })
  .then( function( result ) {

    res.send({ success: true, smfm_item: result });
    return next();
  })
  .catch( function( err ) {

    res.send({ success: false, msg: err });
    return next();
  });

});

module.exports = router;
