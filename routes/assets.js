// mactype routes
// --irfan

var Promise = require('bluebird');
var Router = require('restify-router').Router;  // Restify router modules
var moment = require('moment');  // Date Time Modules
var validate = require("validate.js");  // Data validator

var userbank = require( appRoot + '/lib/userbank' );
var db = require( appRoot + '/lib/db' );  // Load knex sql builder

const router = new Router();  // Create new router

// Check Master Assets sn
function checkName ( sn ) {

  return new Promise ( ( resolve, reject ) => {

    if ( sn ) {

      db( 'assets' )
      .where( 'sn', sn )
      .select( 'id' )
      .limit(1)
      .asCallback( function ( err, rows ){

        if ( err ) {
          reject( err );
          return;
        };

        if ( rows[0] ) {

          reject( {
            sn: [
              'SN has been exists.'
            ]
          });
          return;
        } else {
          resolve();
        };

      });  // Callback

    } else {
      resolve();
    }

  });  // Promise

};

// Check new Asset
function checkNewAsset( new_asset ) {
  
    return function () {
  
      return new Promise ( ( resolve, reject ) => {
  
        var rules = {
          name: {
            presence: {
              allowEmpty: false
            },
            length: {
              maximum: 40
            }
          },
          sn: {
            presence: {
              allowEmpty: false
            },
            length: {
              maximum: 20
            }
          },
          process: {
            presence: {
              allowEmpty: false
            },
            length: {
              maximum: 40
            }
          },
          location_id: {
            presence: {
              allowEmpty: false
            },
            numericality: {
              onlyInteger: true
            }
          }
        };
  
        var err = validate( new_asset, rules);
  
        if ( err ) {
          reject( err );
          return;
        }
  
        checkName( new_asset.name )
        .then( function () {
          resolve( new_asset );
        })
        .catch( function ( err ) {
          reject( err );
        });
  
      });  // Promise
    }  // function
  
  };

// List all Assets
router.get('/', ( req, res, next ) => {

  var cookies = req.cookies;

  var username = cookies.username;
  var login_token = cookies.login_token;

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  userbank.token( username, login_token )
  .then( userbank.checkPermission_g( 'assets_master_manage' ) )

  .then( function() {

    return db( 'v_assets' )
    .select( '*' );
  })
  .then( function( rows ) {

    res.send({ success: true, data: rows });
    return next();
  })
  .catch( function( err ) {

    res.send({ success: false, msg: err });
    return next();
  });

});

// Create new Asset
router.post( '/', ( req, res, next ) => {

  var cookies = req.cookies;

  var username = cookies.username;
  var login_token = cookies.login_token;
  var new_asset = req.body;

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  userbank.token( username, login_token )
  .then( userbank.checkPermission_g( 'assets_master_manage' ) )
  .then( checkNewAsset( new_asset ) )  // Check Asset vars
  .then( function( data ) {

    return db( 'assets' )
    .returning( '*' )
    .insert( data );

  })
  .then( function( r ) {

    res.send({ success: true, new_asset: r });
    return next();
  })
  .catch( function( err ) {

    res.send({ success: false, msg: err });
    return next();
  });

});

// Delete Master Asset
router.del( '/', ( req, res, next ) => {

  var cookies = req.cookies;

  var username = cookies.username;
  var login_token = cookies.login_token;
  var asset_id = req.body.id;
  var asset = {};

  if ( !username || !login_token ) {
    res.send({ success: false });
    return next();
  };

  userbank.token( username, login_token )
  .then( function(user) {
    asset.deleted_at = moment();

    return user;
  })
  .then( userbank.checkPermission_g( 'assets_master_manage' ) )
  .then( function() {

    var q = db( 'assets' );
    if ( typeof asset_id === 'object' ) {
      q = q.whereIn( 'id', asset_id )
    } else {
      q = q.where( 'id', asset_id )
    }

    return q.returning( '*' )
    .update( asset );
  })
  .then( function( result ) {

    res.send({ success: true, asset: result });
    return next();
  });

});

module.exports = router;
